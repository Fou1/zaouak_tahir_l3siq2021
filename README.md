# zaouak_tahir_L3SIQ2021

#phase1
import pandas as pd
import numpy as np

dataset=pd.read_csv('Phase1.csv')

#Faites une analyse exploratoire des données: (valeurs manquantes, données redondantes,transformations des données,...etc.)
dataset.isnull().sum()

Unnamed: 0                     0
duration                       0
protocol_type                  0
service                        0
flag                           0
src_bytes                      0
dst_bytes                      0
land                           0
wrong_fragment                 0
urgent                         0
hot                            0
num_failed_logins              0
logged_in                      0
num_compromised                0
root_shell                     0
su_attempted                   0
num_root                       0
num_file_creations             0
num_shells                     0
num_access_files               0
num_outbound_cmds              0
is_host_login                  0
is_guest_login                 0
count                          0
srv_count                      0
serror_rate                    0
srv_serror_rate                0
rerror_rate                    0
srv_rerror_rate                0
same_srv_rate                  0
diff_srv_rate                  0
srv_diff_host_rate             0
dst_host_count                 0
dst_host_srv_count             0
dst_host_same_srv_rate         0
dst_host_diff_srv_rate         0
dst_host_same_src_port_rate    0
dst_host_srv_diff_host_rate    0
dst_host_serror_rate           0
dst_host_srv_serror_rate       0
dst_host_rerror_rate           0
dst_host_srv_rerror_rate       0
class                          0
dtype: int64
dataset
Unnamed: 0	duration	protocol_type	service	flag	src_bytes	dst_bytes	land	wrong_fragment	urgent	...	dst_host_srv_count	dst_host_same_srv_rate	dst_host_diff_srv_rate	dst_host_same_src_port_rate	dst_host_srv_diff_host_rate	dst_host_serror_rate	dst_host_srv_serror_rate	dst_host_rerror_rate	dst_host_srv_rerror_rate	class
0	0	0	tcp	ftp_data	SF	491	0	0	0	0	...	25	0.17	0.03	0.17	0.00	0.00	0.00	0.05	0.00	normal
1	1	0	udp	other	SF	146	0	0	0	0	...	1	0.00	0.60	0.88	0.00	0.00	0.00	0.00	0.00	normal
2	3	0	tcp	http	SF	232	8153	0	0	0	...	255	1.00	0.00	0.03	0.04	0.03	0.01	0.00	0.01	normal
3	4	0	tcp	http	SF	199	420	0	0	0	...	255	1.00	0.00	0.00	0.00	0.00	0.00	0.00	0.00	normal
4	12	0	tcp	http	SF	287	2251	0	0	0	...	219	1.00	0.00	0.12	0.03	0.00	0.00	0.00	0.00	normal
...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...
13594	310	0	tcp	uucp	S0	0	0	0	0	0	...	13	0.05	0.07	0.00	0.00	1.00	1.00	0.00	0.00	anomaly
13595	312	0	tcp	exec	RSTO	0	0	0	0	0	...	9	0.04	0.06	0.00	0.00	0.00	0.00	1.00	1.00	anomaly
13596	313	0	tcp	netbios_dgm	S0	0	0	0	0	0	...	19	0.07	0.07	0.00	0.00	1.00	1.00	0.00	0.00	anomaly
13597	315	0	icmp	ecr_i	SF	1032	0	0	0	0	...	255	1.00	0.00	1.00	0.00	0.00	0.00	0.00	0.00	anomaly
13598	316	0	icmp	eco_i	SF	8	0	0	0	0	...	11	1.00	0.00	1.00	0.55	0.00	0.00	0.00	0.00	anomaly
13599 rows × 43 columns

dataset=dataset.drop_duplicates(keep='first')
dataset

Unnamed: 0	duration	protocol_type	service	flag	src_bytes	dst_bytes	land	wrong_fragment	urgent	...	dst_host_srv_count	dst_host_same_srv_rate	dst_host_diff_srv_rate	dst_host_same_src_port_rate	dst_host_srv_diff_host_rate	dst_host_serror_rate	dst_host_srv_serror_rate	dst_host_rerror_rate	dst_host_srv_rerror_rate	class
0	0	0	tcp	ftp_data	SF	491	0	0	0	0	...	25	0.17	0.03	0.17	0.00	0.00	0.00	0.05	0.00	normal
1	1	0	udp	other	SF	146	0	0	0	0	...	1	0.00	0.60	0.88	0.00	0.00	0.00	0.00	0.00	normal
2	3	0	tcp	http	SF	232	8153	0	0	0	...	255	1.00	0.00	0.03	0.04	0.03	0.01	0.00	0.01	normal
3	4	0	tcp	http	SF	199	420	0	0	0	...	255	1.00	0.00	0.00	0.00	0.00	0.00	0.00	0.00	normal
4	12	0	tcp	http	SF	287	2251	0	0	0	...	219	1.00	0.00	0.12	0.03	0.00	0.00	0.00	0.00	normal
...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...
13594	310	0	tcp	uucp	S0	0	0	0	0	0	...	13	0.05	0.07	0.00	0.00	1.00	1.00	0.00	0.00	anomaly
13595	312	0	tcp	exec	RSTO	0	0	0	0	0	...	9	0.04	0.06	0.00	0.00	0.00	0.00	1.00	1.00	anomaly
13596	313	0	tcp	netbios_dgm	S0	0	0	0	0	0	...	19	0.07	0.07	0.00	0.00	1.00	1.00	0.00	0.00	anomaly
13597	315	0	icmp	ecr_i	SF	1032	0	0	0	0	...	255	1.00	0.00	1.00	0.00	0.00	0.00	0.00	0.00	anomaly
13598	316	0	icmp	eco_i	SF	8	0	0	0	0	...	11	1.00	0.00	1.00	0.55	0.00	0.00	0.00	0.00	anomaly
13599 rows × 43 columns


dataset.describe()
dataset.describe()

Unnamed: 0	duration	src_bytes	dst_bytes	land	wrong_fragment	urgent	hot	num_failed_logins	logged_in	...	dst_host_count	dst_host_srv_count	dst_host_same_srv_rate	dst_host_diff_srv_rate	dst_host_same_src_port_rate	dst_host_srv_diff_host_rate	dst_host_serror_rate	dst_host_srv_serror_rate	dst_host_rerror_rate	dst_host_srv_rerror_rate
count	13599.000000	13599.000000	1.359900e+04	1.359900e+04	13599.000000	13599.000000	13599.0	13599.000000	13599.000000	13599.000000	...	13599.000000	13599.000000	13599.000000	13599.000000	13599.000000	13599.000000	13599.000000	13599.000000	13599.000000	13599.000000
mean	12495.645636	182.747776	1.164666e+04	4.352227e+03	0.000074	0.001177	0.0	0.222149	0.001177	0.701669	...	149.209795	187.626664	0.803486	0.041635	0.121749	0.025695	0.021073	0.013594	0.046417	0.044595
std	7333.313621	1371.063164	1.723699e+05	6.941981e+04	0.008575	0.058150	0.0	2.340105	0.043711	0.457542	...	101.645694	94.248057	0.331422	0.130418	0.255221	0.068712	0.122787	0.101307	0.195325	0.191117
min	0.000000	0.000000	0.000000e+00	0.000000e+00	0.000000	0.000000	0.0	0.000000	0.000000	0.000000	...	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000
25%	6113.500000	0.000000	1.050000e+02	8.100000e+01	0.000000	0.000000	0.0	0.000000	0.000000	0.000000	...	41.000000	110.000000	0.720000	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000
50%	12521.000000	0.000000	2.320000e+02	3.700000e+02	0.000000	0.000000	0.0	0.000000	0.000000	1.000000	...	161.000000	255.000000	1.000000	0.000000	0.010000	0.000000	0.000000	0.000000	0.000000	0.000000
75%	18848.000000	0.000000	3.240000e+02	2.009000e+03	0.000000	0.000000	0.0	0.000000	0.000000	1.000000	...	255.000000	255.000000	1.000000	0.020000	0.080000	0.030000	0.000000	0.000000	0.000000	0.000000
max	25186.000000	36613.000000	7.665876e+06	5.131424e+06	1.000000	3.000000	0.0	77.000000	3.000000	1.000000	...	255.000000	255.000000	1.000000	1.000000	1.000000	1.000000	1.000000	1.000000	1.000000	1.000000
8 rows × 39 columns
